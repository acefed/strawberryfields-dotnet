# StrawberryFields .NET

- [2.0.0]
- [1.2.0] - 2024-08-10 - 9 files changed, 271 insertions(+), 169 deletions(-)
- [1.1.0] - 2024-03-10 - 6 files changed, 538 insertions(+), 57 deletions(-)
- 1.0.0 - 2024-02-11

[2.0.0]: https://gitlab.com/acefed/strawberryfields-dotnet/-/compare/7255f0ce...main
[1.2.0]: https://gitlab.com/acefed/strawberryfields-dotnet/-/compare/8dacf961...7255f0ce
[1.1.0]: https://gitlab.com/acefed/strawberryfields-dotnet/-/compare/20efec81...8dacf961
