FROM mcr.microsoft.com/dotnet/sdk:8.0-azurelinux3.0 AS builder

WORKDIR /app
COPY . .

RUN dotnet publish -c Release -o publish

FROM mcr.microsoft.com/dotnet/aspnet:8.0-azurelinux3.0-distroless

WORKDIR /app
COPY --from=builder /app/data ./data
COPY --from=builder /app/publish .

ENTRYPOINT ["/app/StrawberryFieldsDotNet"]
