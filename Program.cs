using System.Security.Cryptography;
using System.Text;
using System.Text.Encodings.Web;
using System.Text.Json;
using System.Text.Json.Nodes;
using System.Text.Unicode;

using dotenv.net;

DotEnv.Load();
var port = Environment.GetEnvironmentVariable("PORT") ?? "8080";
var enc = new JsonSerializerOptions();
enc.Encoder = JavaScriptEncoder.UnsafeRelaxedJsonEscaping;
string configJson;
if (!String.IsNullOrEmpty(Environment.GetEnvironmentVariable("CONFIG_JSON")))
{
    configJson = Environment.GetEnvironmentVariable("CONFIG_JSON")!;
    if (configJson.StartsWith("'"))
        configJson = configJson.Substring(1);
    if (configJson.EndsWith("'"))
        configJson = configJson.Substring(0, configJson.Length - 1);
}
else
    configJson = File.ReadAllText("data/config.json");
var config = JsonNode.Parse(configJson)!;
var me = String.Join("", new List<string>
{
    "<a href=\"https://",
    new Uri(config["actor"]?[0]?["me"]!.ToString()!).Host,
    "/\" rel=\"me nofollow noopener noreferrer\" target=\"_blank\">",
    "https://",
    new Uri(config["actor"]?[0]?["me"]!.ToString()!).Host,
    "/",
    "</a>"
});
var privateKeyPem = Environment.GetEnvironmentVariable("PRIVATE_KEY")!;
privateKeyPem = privateKeyPem.Replace("\\n", "\n");
if (privateKeyPem.StartsWith("\""))
    privateKeyPem = privateKeyPem.Substring(1);
if (privateKeyPem.EndsWith("\""))
    privateKeyPem = privateKeyPem.Substring(0, privateKeyPem.Length - 1);
var privateKey = privateKeyPem;
var publicKeyPem = ExportPublicKey(privateKey);

var builder = WebApplication.CreateBuilder(args);
var app = builder.Build();
app.UseStaticFiles();
app.UseStatusCodePages();

string ExportPublicKey(string key)
{
    var der = RSA.Create();
    der.ImportFromPem(key);
    var b64 = Convert.ToBase64String(der.ExportSubjectPublicKeyInfo());
    var pem = "-----BEGIN PUBLIC KEY-----" + "\n";
    while (b64.Length > 64)
    {
        pem += b64.Substring(0, 64) + "\n";
        b64 = b64.Substring(64);
    }
    pem += b64.Substring(0, b64.Length) + "\n";
    pem += "-----END PUBLIC KEY-----" + "\n";
    return pem;
}

string Uuidv7()
{
    var rand = RandomNumberGenerator.Create();
    var v = new byte[16];
    rand.GetBytes(v);
    var ts = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();
    v[0] = (byte)(ts >> 40);
    v[1] = (byte)(ts >> 32);
    v[2] = (byte)(ts >> 24);
    v[3] = (byte)(ts >> 16);
    v[4] = (byte)(ts >> 8);
    v[5] = (byte)ts;
    v[6] = (byte)(v[6] & 0x0F | 0x70);
    v[8] = (byte)(v[8] & 0x3F | 0x80);
    return Convert.ToHexString(v).ToLower();
}

string TalkScript(string req)
{
    var ts = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds().ToString();
    if (new Uri(req).Host == "localhost")
        return $"<p>{ts}</p>";
    return String.Join("", new List<string>
    {
        "<p>",
        "<a href=\"https://",
        new Uri(req).Host,
        "/\" rel=\"nofollow noopener noreferrer\" target=\"_blank\">",
        new Uri(req).Host,
        "</a>",
        "</p>"
    });
}

async Task<string> GetActivity(string username, string hostname, string req)
{
    var t = DateTime.UtcNow.ToString("r");
    var sig = RSA.Create();
    sig.ImportFromPem(privateKey);
    var b64 = Convert.ToBase64String(sig.SignData(Encoding.UTF8.GetBytes(String.Join("\n", new List<string>
    {
        $"(request-target): get {new Uri(req).AbsolutePath}",
        $"host: {new Uri(req).Host}",
        $"date: {t}"
    })), HashAlgorithmName.SHA256, RSASignaturePadding.Pkcs1));
    var headers = new Dictionary<string, string>
    {
        { "Date", t },
        { "Signature", String.Join(",", new List<string>
            {
                $"keyId=\"https://{hostname}/u/{username}#Key\"",
                "algorithm=\"rsa-sha256\"",
                "headers=\"(request-target) host date\"",
                $"signature=\"{b64}\""
            }
        )},
        { "Accept", "application/activity+json" },
        { "Accept-Encoding", "identity" },
        { "Cache-Control", "no-cache" },
        { "User-Agent", $"StrawberryFields-DotNet/2.0.0 (+https://{hostname}/)" }
    };
    var client = new HttpClient();
    foreach (var header in headers)
        client.DefaultRequestHeaders.Add(header.Key, header.Value);
    var res = await client.GetAsync(req);
    var status = (int)res.StatusCode;
    Console.WriteLine($"GET {req} {status}");
    var body = await res.Content.ReadAsStringAsync();
    return body;
}

async Task PostActivity(string username, string hostname, string req, Dictionary<string, object> x)
{
    var t = DateTime.UtcNow.ToString("r");
    var body = JsonSerializer.Serialize(x, enc);
    var s256 = Convert.ToBase64String(SHA256.HashData(Encoding.UTF8.GetBytes(body)));
    var sig = RSA.Create();
    sig.ImportFromPem(privateKey);
    var b64 = Convert.ToBase64String(sig.SignData(Encoding.UTF8.GetBytes(String.Join("\n", new List<string>
    {
        $"(request-target): post {new Uri(req).AbsolutePath}",
        $"host: {new Uri(req).Host}",
        $"date: {t}",
        $"digest: SHA-256={s256}"
    })), HashAlgorithmName.SHA256, RSASignaturePadding.Pkcs1));
    var headers = new Dictionary<string, string>
    {
        { "Date", t },
        { "Digest", $"SHA-256={s256}" },
        { "Signature", String.Join(",", new List<string>
            {
                $"keyId=\"https://{hostname}/u/{username}#Key\"",
                "algorithm=\"rsa-sha256\"",
                "headers=\"(request-target) host date digest\"",
                $"signature=\"{b64}\""
            }
        )},
        { "Accept", "application/json" },
        { "Accept-Encoding", "gzip" },
        { "Cache-Control", "max-age=0" },
        { "User-Agent", $"StrawberryFields-DotNet/2.0.0 (+https://{hostname}/)" }
    };
    var client = new HttpClient();
    foreach (var header in headers)
        client.DefaultRequestHeaders.Add(header.Key, header.Value);
    Console.WriteLine($"POST {req} {body}");
    await client.PostAsync(req, new StringContent(body, Encoding.UTF8, "application/activity+json"));
}

async Task AcceptFollow(string username, string hostname, JsonNode x, JsonNode y)
{
    var aid = Uuidv7();
    var body = new Dictionary<string, object>
    {
        { "@context", "https://www.w3.org/ns/activitystreams" },
        { "id", $"https://{hostname}/u/{username}/s/{aid}" },
        { "type", "Accept" },
        { "actor", $"https://{hostname}/u/{username}" },
        { "object", y }
    };
    await PostActivity(username, hostname, x["inbox"]!.ToString(), body);
}

async Task Follow(string username, string hostname, JsonNode x)
{
    var aid = Uuidv7();
    var body = new Dictionary<string, object>
    {
        { "@context", "https://www.w3.org/ns/activitystreams" },
        { "id", $"https://{hostname}/u/{username}/s/{aid}" },
        { "type", "Follow" },
        { "actor", $"https://{hostname}/u/{username}" },
        { "object", x["id"]!.ToString() }
    };
    await PostActivity(username, hostname, x["inbox"]!.ToString(), body);
}

async Task UndoFollow(string username, string hostname, JsonNode x)
{
    var aid = Uuidv7();
    var body = new Dictionary<string, object>
    {
        { "@context", "https://www.w3.org/ns/activitystreams" },
        { "id", $"https://{hostname}/u/{username}/s/{aid}#Undo" },
        { "type", "Undo" },
        { "actor", $"https://{hostname}/u/{username}" },
        { "object", new Dictionary<string, string>
            {
                { "id", $"https://{hostname}/u/{username}/s/{aid}" },
                { "type", "Follow" },
                { "actor", $"https://{hostname}/u/{username}" },
                { "object", x["id"]!.ToString() }
            }
        }
    };
    await PostActivity(username, hostname, x["inbox"]!.ToString(), body);
}

async Task Like(string username, string hostname, JsonNode x, JsonNode y)
{
    var aid = Uuidv7();
    var body = new Dictionary<string, object>
    {
        { "@context", "https://www.w3.org/ns/activitystreams" },
        { "id", $"https://{hostname}/u/{username}/s/{aid}" },
        { "type", "Like" },
        { "actor", $"https://{hostname}/u/{username}" },
        { "object", x["id"]!.ToString() }
    };
    await PostActivity(username, hostname, y["inbox"]!.ToString(), body);
}

async Task UndoLike(string username, string hostname, JsonNode x, JsonNode y)
{
    var aid = Uuidv7();
    var body = new Dictionary<string, object>
    {
        { "@context", "https://www.w3.org/ns/activitystreams" },
        { "id", $"https://{hostname}/u/{username}/s/{aid}#Undo" },
        { "type", "Undo" },
        { "actor", $"https://{hostname}/u/{username}" },
        { "object", new Dictionary<string, string>
            {
                { "id", $"https://{hostname}/u/{username}/s/{aid}" },
                { "type", "Like" },
                { "actor", $"https://{hostname}/u/{username}" },
                { "object", x["id"]!.ToString() },
            }
        }
    };
    await PostActivity(username, hostname, y["inbox"]!.ToString(), body);
}

async Task Announce(string username, string hostname, JsonNode x, JsonNode y)
{
    var aid = Uuidv7();
    var t = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ssZ");
    var body = new Dictionary<string, object>
    {
        { "@context", "https://www.w3.org/ns/activitystreams" },
        { "id", $"https://{hostname}/u/{username}/s/{aid}/activity" },
        { "type", "Announce" },
        { "actor", $"https://{hostname}/u/{username}" },
        { "published", t },
        { "to", new List<string> { "https://www.w3.org/ns/activitystreams#Public" } },
        { "cc", new List<string> { $"https://{hostname}/u/{username}/followers" } },
        { "object", x["id"]!.ToString() }
    };
    await PostActivity(username, hostname, y["inbox"]!.ToString(), body);
}

async Task UndoAnnounce(string username, string hostname, JsonNode x, JsonNode y, string z)
{
    var aid = Uuidv7();
    var body = new Dictionary<string, object>
    {
        { "@context", "https://www.w3.org/ns/activitystreams" },
        { "id", $"https://{hostname}/u/{username}/s/{aid}#Undo" },
        { "type", "Undo" },
        { "actor", $"https://{hostname}/u/{username}" },
        { "object", new Dictionary<string, object>
            {
                { "id", $"{z}/activity" },
                { "type", "Announce" },
                { "actor", $"https://{hostname}/u/{username}" },
                { "to", new List<string> { "https://www.w3.org/ns/activitystreams#Public" } },
                { "cc", new List<string> { $"https://{hostname}/u/{username}/followers" } },
                { "object", x["id"]!.ToString() }
            }
        }
    };
    await PostActivity(username, hostname, y["inbox"]!.ToString(), body);
}

async Task CreateNote(string username, string hostname, JsonNode x, string y)
{
    var aid = Uuidv7();
    var t = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ssZ");
    var body = new Dictionary<string, object>
    {
        { "@context", "https://www.w3.org/ns/activitystreams" },
        { "id", $"https://{hostname}/u/{username}/s/{aid}/activity" },
        { "type", "Create" },
        { "actor", $"https://{hostname}/u/{username}" },
        { "published", t },
        { "to", new List<string> { "https://www.w3.org/ns/activitystreams#Public" } },
        { "cc", new List<string> { $"https://{hostname}/u/{username}/followers" } },
        { "object", new Dictionary<string, object>
            {
                { "id", $"https://{hostname}/u/{username}/s/{aid}" },
                { "type", "Note" },
                { "attributedTo", $"https://{hostname}/u/{username}" },
                { "content", TalkScript(y) },
                { "url", $"https://{hostname}/u/{username}/s/{aid}" },
                { "published", t },
                { "to", new List<string> { "https://www.w3.org/ns/activitystreams#Public" } },
                { "cc", new List<string> { $"https://{hostname}/u/{username}/followers" } }
            }
        }
    };
    await PostActivity(username, hostname, x["inbox"]!.ToString(), body);
}

async Task CreateNoteImage(string username, string hostname, JsonNode x, string y, string z)
{
    var aid = Uuidv7();
    var t = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ssZ");
    var body = new Dictionary<string, object>
    {
        { "@context", "https://www.w3.org/ns/activitystreams" },
        { "id", $"https://{hostname}/u/{username}/s/{aid}/activity" },
        { "type", "Create" },
        { "actor", $"https://{hostname}/u/{username}" },
        { "published", t },
        { "to", new List<string> { "https://www.w3.org/ns/activitystreams#Public" } },
        { "cc", new List<string> { $"https://{hostname}/u/{username}/followers" } },
        { "object", new Dictionary<string, object>
            {
                { "id", $"https://{hostname}/u/{username}/s/{aid}" },
                { "type", "Note" },
                { "attributedTo", $"https://{hostname}/u/{username}" },
                { "content", TalkScript("https://localhost") },
                { "url", $"https://{hostname}/u/{username}/s/{aid}" },
                { "published", t },
                { "to", new List<string> { "https://www.w3.org/ns/activitystreams#Public" } },
                { "cc", new List<string> { $"https://{hostname}/u/{username}/followers" } },
                { "attachment", new List<object>
                    {
                        new Dictionary<string, string>
                        {
                            { "type", "Image" },
                            { "mediaType", z },
                            { "url", y }
                        }
                    }
                }
            }
        }
    };
    await PostActivity(username, hostname, x["inbox"]!.ToString(), body);
}

async Task CreateNoteMention(string username, string hostname, JsonNode x, JsonNode y, string z)
{
    var aid = Uuidv7();
    var t = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ssZ");
    var at = $"@{y["preferredUsername"]!.ToString()}@{new Uri(y["inbox"]!.ToString()).Host}";
    var body = new Dictionary<string, object>
    {
        { "@context", "https://www.w3.org/ns/activitystreams" },
        { "id", $"https://{hostname}/u/{username}/s/{aid}/activity" },
        { "type", "Create" },
        { "actor", $"https://{hostname}/u/{username}" },
        { "published", t },
        { "to", new List<string> { "https://www.w3.org/ns/activitystreams#Public" } },
        { "cc", new List<string> { $"https://{hostname}/u/{username}/followers" } },
        { "object", new Dictionary<string, object>
            {
                { "id", $"https://{hostname}/u/{username}/s/{aid}" },
                { "type", "Note" },
                { "attributedTo", $"https://{hostname}/u/{username}" },
                { "inReplyTo", x["id"]!.ToString() },
                { "content", TalkScript(z) },
                { "url", $"https://{hostname}/u/{username}/s/{aid}" },
                { "published", t },
                { "to", new List<string> { "https://www.w3.org/ns/activitystreams#Public" } },
                { "cc", new List<string> { $"https://{hostname}/u/{username}/followers" } },
                { "tag", new List<object>
                    {
                        new Dictionary<string, string>
                        {
                            { "type", "Mention" },
                            { "name", at }
                        }
                    }
                }
            }
        }
    };
    await PostActivity(username, hostname, y["inbox"]!.ToString(), body);
}

async Task CreateNoteHashtag(string username, string hostname, JsonNode x, string y, string z)
{
    var aid = Uuidv7();
    var t = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ssZ");
    var body = new Dictionary<string, object>
    {
        { "@context", new List<object>
            {
                "https://www.w3.org/ns/activitystreams",
                new Dictionary<string, string> { { "Hashtag", "as:Hashtag" } }
            }
        },
        { "id", $"https://{hostname}/u/{username}/s/{aid}/activity" },
        { "type", "Create" },
        { "actor", $"https://{hostname}/u/{username}" },
        { "published", t },
        { "to", new List<string> { "https://www.w3.org/ns/activitystreams#Public" } },
        { "cc", new List<string> { $"https://{hostname}/u/{username}/followers" } },
        { "object", new Dictionary<string, object>
            {
                { "id", $"https://{hostname}/u/{username}/s/{aid}" },
                { "type", "Note" },
                { "attributedTo", $"https://{hostname}/u/{username}" },
                { "content", TalkScript(y) },
                { "url", $"https://{hostname}/u/{username}/s/{aid}" },
                { "published", t },
                { "to", new List<string> { "https://www.w3.org/ns/activitystreams#Public" } },
                { "cc", new List<string> { $"https://{hostname}/u/{username}/followers" } },
                { "tag", new List<object>
                    {
                        new Dictionary<string, string>
                        {
                            { "type", "Hashtag" },
                            { "name", $"#{z}" }
                        }
                    }
                }
            }
        }
    };
    await PostActivity(username, hostname, x["inbox"]!.ToString(), body);
}

async Task UpdateNote(string username, string hostname, JsonNode x, string y)
{
    var t = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ssZ");
    var ts = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds().ToString();
    var body = new Dictionary<string, object>
    {
        { "@context", "https://www.w3.org/ns/activitystreams" },
        { "id", $"{y}#{ts}" },
        { "type", "Update" },
        { "actor", $"https://{hostname}/u/{username}" },
        { "published", t },
        { "to", new List<string> { "https://www.w3.org/ns/activitystreams#Public" } },
        { "cc", new List<string> { $"https://{hostname}/u/{username}/followers" } },
        { "object", new Dictionary<string, object>
            {
                { "id", y },
                { "type", "Note" },
                { "attributedTo", $"https://{hostname}/u/{username}" },
                { "content", TalkScript("https://localhost") },
                { "url", y },
                { "updated", t },
                { "to", new List<string> { "https://www.w3.org/ns/activitystreams#Public" } },
                { "cc", new List<string> { $"https://{hostname}/u/{username}/followers" } }
            }
        }
    };
    await PostActivity(username, hostname, x["inbox"]!.ToString(), body);
}

async Task DeleteTombstone(string username, string hostname, JsonNode x, string y)
{
    var body = new Dictionary<string, object>
    {
        { "@context", "https://www.w3.org/ns/activitystreams" },
        { "id", $"{y}#Delete" },
        { "type", "Delete" },
        { "actor", $"https://{hostname}/u/{username}" },
        { "object", new Dictionary<string, string>
            {
                { "id", y },
                { "type", "Tombstone" }
            }
        }
    };
    await PostActivity(username, hostname, x["inbox"]!.ToString(), body);
}

app.MapGet("/", () => Results.Text("StrawberryFields .NET"));

app.MapGet("/about", () => Results.Text("About: Blank"));

app.MapGet("/u/{username}", (string username, HttpContext context) =>
{
    object body;
    Dictionary<string, string> headers;
    var hostname = new Uri(config["origin"]!.ToString()).Host;
    var acceptHeaderField = context.Request.Headers["Accept"].ToString();
    var hasType = false;
    if (username != config["actor"]?[0]?["preferredUsername"]!.ToString())
        return Results.NotFound();
    if (acceptHeaderField.Contains("application/activity+json"))
        hasType = true;
    if (acceptHeaderField.Contains("application/ld+json"))
        hasType = true;
    if (acceptHeaderField.Contains("application/json"))
        hasType = true;
    if (!hasType)
    {
        body = $"{username}: {config["actor"]?[0]?["name"]!.ToString()}";
        headers = new Dictionary<string, string>
        {
            { "Cache-Control", $"public, max-age={config["ttl"]!.ToString()}, must-revalidate" },
            { "Vary", "Accept, Accept-Encoding" }
        };
        foreach (var header in headers)
            context.Response.Headers[header.Key] = header.Value;
        return Results.Text(body.ToString());
    }
    body = new Dictionary<string, object>
    {
        { "@context", new List<object>
            {
                "https://www.w3.org/ns/activitystreams",
                "https://w3id.org/security/v1",
                new Dictionary<string, string>
                {
                    { "schema", "https://schema.org/" },
                    { "PropertyValue", "schema:PropertyValue" },
                    { "value", "schema:value" },
                    { "Key", "sec:Key" }
                }
            }
        },
        { "id", $"https://{hostname}/u/{username}" },
        { "type", "Person" },
        { "inbox", $"https://{hostname}/u/{username}/inbox" },
        { "outbox", $"https://{hostname}/u/{username}/outbox" },
        { "following", $"https://{hostname}/u/{username}/following" },
        { "followers", $"https://{hostname}/u/{username}/followers" },
        { "preferredUsername", username },
        { "name", config["actor"]?[0]?["name"]!.ToString()! },
        { "summary", "<p>2.0.0</p>" },
        { "url", $"https://{hostname}/u/{username}" },
        { "endpoints", new Dictionary<string, string> { { "sharedInbox", $"https://{hostname}/u/{username}/inbox" } } },
        { "attachment", new List<object>
            {
                new Dictionary<string, string>
                {
                    { "type", "PropertyValue" },
                    { "name", "me" },
                    { "value", me }
                }
            }
        },
        { "icon", new Dictionary<string, string>
            {
                { "type", "Image" },
                { "mediaType", "image/png" },
                { "url", $"https://{hostname}/static/{username}u.png" }
            }
        },
        { "image", new Dictionary<string, string>
            {
                { "type", "Image" },
                { "mediaType", "image/png" },
                { "url", $"https://{hostname}/static/{username}s.png" }
            }
        },
        { "publicKey", new Dictionary<string, string>
            {
                { "id", $"https://{hostname}/u/{username}#Key" },
                { "type", "Key" },
                { "owner", $"https://{hostname}/u/{username}" },
                { "publicKeyPem", publicKeyPem }
            }
        }
    };
    headers = new Dictionary<string, string>
    {
        { "Cache-Control", $"public, max-age={config["ttl"]!.ToString()}, must-revalidate" },
        { "Vary", "Accept, Accept-Encoding" }
    };
    foreach (var header in headers)
        context.Response.Headers[header.Key] = header.Value;
    return Results.Json(body, contentType: "application/activity+json");
});

app.MapPost("/u/{username}/inbox", async (string username, HttpContext context) =>
{
    var hostname = new Uri(config["origin"]!.ToString()).Host;
    var contentTypeHeaderField = context.Request.Headers["Content-Type"].ToString();
    var hasType = false;
    var y = await new StreamReader(context.Request.Body).ReadToEndAsync();
    var ny = JsonNode.Parse(y)!;
    var t = ny["type"]?.GetValue<string>() ?? "";
    var aid = ny["id"]?.GetValue<string>() ?? "";
    var atype = ny["type"]?.GetValue<string>() ?? "";
    if (aid.Length > 1024 || atype.Length > 64)
        return Results.BadRequest();
    Console.WriteLine($"INBOX {aid} {atype}");
    if (username != config["actor"]?[0]?["preferredUsername"]!.ToString())
        return Results.NotFound();
    if (contentTypeHeaderField.Contains("application/activity+json"))
        hasType = true;
    if (contentTypeHeaderField.Contains("application/ld+json"))
        hasType = true;
    if (contentTypeHeaderField.Contains("application/json"))
        hasType = true;
    if (!hasType)
        return Results.BadRequest();
    if (String.IsNullOrEmpty(context.Request.Headers["Digest"]))
        return Results.BadRequest();
    if (String.IsNullOrEmpty(context.Request.Headers["Signature"]))
        return Results.BadRequest();
    if (t == "Accept" || t == "Reject" || t == "Add")
        return Results.Ok();
    if (t == "Remove" || t == "Like" || t == "Announce")
        return Results.Ok();
    if (t == "Create" || t == "Update" || t == "Delete")
        return Results.Ok();
    if (t == "Follow")
    {
        if (new Uri(ny["actor"]?.GetValue<string>() ?? "about:blank").Scheme != "https")
            return Results.BadRequest();
        var x = await GetActivity(username, hostname, ny["actor"]!.ToString());
        if (String.IsNullOrEmpty(x))
            return Results.StatusCode(500);
        var nx = JsonNode.Parse(x)!;
        await AcceptFollow(username, hostname, nx, ny);
        return Results.Ok();
    }
    if (t == "Undo")
    {
        var nz = JsonNode.Parse(ny["object"]?.GetValue<string>() ?? "{}")!;
        t = nz["type"]?.GetValue<string>() ?? "";
        if (t == "Accept" || t == "Like" || t == "Announce")
            return Results.Ok();
        if (t == "Follow")
        {
            if (new Uri(ny["actor"]?.GetValue<string>() ?? "about:blank").Scheme != "https")
                return Results.BadRequest();
            var x = await GetActivity(username, hostname, ny["actor"]!.ToString());
            if (String.IsNullOrEmpty(x))
                return Results.StatusCode(500);
            var nx = JsonNode.Parse(x)!;
            await AcceptFollow(username, hostname, nx, nz);
            return Results.Ok();
        }
    }
    return Results.StatusCode(500);
});

app.MapGet("/u/{username}/outbox", (string username) =>
{
    var hostname = new Uri(config["origin"]!.ToString()).Host;
    if (username != config["actor"]?[0]?["preferredUsername"]!.ToString())
        return Results.NotFound();
    var body = new Dictionary<string, object>
    {
        { "@context", "https://www.w3.org/ns/activitystreams" },
        { "id", $"https://{hostname}/u/{username}/outbox" },
        { "type", "OrderedCollection" },
        { "totalItems", 0 }
    };
    return Results.Json(body, contentType: "application/activity+json");
});

app.MapGet("/u/{username}/following", (string username) =>
{
    var hostname = new Uri(config["origin"]!.ToString()).Host;
    if (username != config["actor"]?[0]?["preferredUsername"]!.ToString())
        return Results.NotFound();
    var body = new Dictionary<string, object>
    {
        { "@context", "https://www.w3.org/ns/activitystreams" },
        { "id", $"https://{hostname}/u/{username}/following" },
        { "type", "OrderedCollection" },
        { "totalItems", 0 }
    };
    return Results.Json(body, contentType: "application/activity+json");
});

app.MapGet("/u/{username}/followers", (string username) =>
{
    var hostname = new Uri(config["origin"]!.ToString()).Host;
    if (username != config["actor"]?[0]?["preferredUsername"]!.ToString())
        return Results.NotFound();
    var body = new Dictionary<string, object>
    {
        { "@context", "https://www.w3.org/ns/activitystreams" },
        { "id", $"https://{hostname}/u/{username}/followers" },
        { "type", "OrderedCollection" },
        { "totalItems", 0 }
    };
    return Results.Json(body, contentType: "application/activity+json");
});

app.MapPost("/s/{secret}/u/{username}", async (string secret, string username, HttpContext context) =>
{
    var hostname = new Uri(config["origin"]!.ToString()).Host;
    var body = await new StreamReader(context.Request.Body).ReadToEndAsync();
    var send = JsonNode.Parse(body)!;
    var t = send["type"]?.GetValue<string>() ?? "";
    if (username != config["actor"]?[0]?["preferredUsername"]!.ToString())
        return Results.NotFound();
    if (String.IsNullOrEmpty(secret) || secret == "-")
        return Results.NotFound();
    if (secret != Environment.GetEnvironmentVariable("SECRET"))
        return Results.NotFound();
    if (new Uri(send["id"]?.GetValue<string>() ?? "about:blank").Scheme != "https")
        return Results.BadRequest();
    var x = await GetActivity(username, hostname, send["id"]!.ToString());
    if (String.IsNullOrEmpty(x))
        return Results.StatusCode(500);
    var nx = JsonNode.Parse(x)!;
    var aid = nx["id"]?.GetValue<string>() ?? "";
    var atype = nx["type"]?.GetValue<string>() ?? "";
    if (aid.Length > 1024 || atype.Length > 64)
        return Results.BadRequest();
    if (t == "follow")
    {
        await Follow(username, hostname, nx);
        return Results.Ok();
    }
    if (t == "undo_follow")
    {
        await UndoFollow(username, hostname, nx);
        return Results.Ok();
    }
    if (t == "like")
    {
        if (new Uri(nx["attributedTo"]?.GetValue<string>() ?? "about:blank").Scheme != "https")
            return Results.BadRequest();
        var y = await GetActivity(username, hostname, nx["attributedTo"]!.ToString());
        if (String.IsNullOrEmpty(y))
            return Results.StatusCode(500);
        var ny = JsonNode.Parse(y)!;
        await Like(username, hostname, nx, ny);
        return Results.Ok();
    }
    if (t == "undo_like")
    {
        if (new Uri(nx["attributedTo"]?.GetValue<string>() ?? "about:blank").Scheme != "https")
            return Results.BadRequest();
        var y = await GetActivity(username, hostname, nx["attributedTo"]!.ToString());
        if (String.IsNullOrEmpty(y))
            return Results.StatusCode(500);
        var ny = JsonNode.Parse(y)!;
        await UndoLike(username, hostname, nx, ny);
        return Results.Ok();
    }
    if (t == "announce")
    {
        if (new Uri(nx["attributedTo"]?.GetValue<string>() ?? "about:blank").Scheme != "https")
            return Results.BadRequest();
        var y = await GetActivity(username, hostname, nx["attributedTo"]!.ToString());
        if (String.IsNullOrEmpty(y))
            return Results.StatusCode(500);
        var ny = JsonNode.Parse(y)!;
        await Announce(username, hostname, nx, ny);
        return Results.Ok();
    }
    if (t == "undo_announce")
    {
        if (new Uri(nx["attributedTo"]?.GetValue<string>() ?? "about:blank").Scheme != "https")
            return Results.BadRequest();
        var y = await GetActivity(username, hostname, nx["attributedTo"]!.ToString());
        if (String.IsNullOrEmpty(y))
            return Results.StatusCode(500);
        var ny = JsonNode.Parse(y)!;
        string z;
        if (!String.IsNullOrEmpty(send["url"]?.GetValue<string>()))
            z = send["url"]!.ToString();
        else
            z = $"https://{hostname}/u/{username}/s/00000000000000000000000000000000";
        if (new Uri(z).Scheme != "https")
            return Results.BadRequest();
        await UndoAnnounce(username, hostname, nx, ny, z);
        return Results.Ok();
    }
    if (t == "create_note")
    {
        string y;
        if (!String.IsNullOrEmpty(send["url"]?.GetValue<string>()))
            y = send["url"]!.ToString();
        else
            y = "https://localhost";
        if (new Uri(y).Scheme != "https")
            return Results.BadRequest();
        await CreateNote(username, hostname, nx, y);
        return Results.Ok();
    }
    if (t == "create_note_image")
    {
        string y;
        if (!String.IsNullOrEmpty(send["url"]?.GetValue<string>()))
            y = send["url"]!.ToString();
        else
            y = $"https://{hostname}/static/logo.png";
        if (new Uri(y).Scheme != "https" || new Uri(y).Host != hostname)
            return Results.BadRequest();
        var z = "image/png";
        if (y.EndsWith(".jpg") || y.EndsWith(".jpeg")) z = "image/jpeg";
        if (y.EndsWith(".svg")) z = "image/svg+xml";
        if (y.EndsWith(".gif")) z = "image/gif";
        if (y.EndsWith(".webp")) z = "image/webp";
        if (y.EndsWith(".avif")) z = "image/avif";
        await CreateNoteImage(username, hostname, nx, y, z);
        return Results.Ok();
    }
    if (t == "create_note_mention")
    {
        if (new Uri(nx["attributedTo"]?.GetValue<string>() ?? "about:blank").Scheme != "https")
            return Results.BadRequest();
        var y = await GetActivity(username, hostname, nx["attributedTo"]!.ToString());
        if (String.IsNullOrEmpty(y))
            return Results.StatusCode(500);
        var ny = JsonNode.Parse(y)!;
        string z;
        if (!String.IsNullOrEmpty(send["url"]?.GetValue<string>()))
            z = send["url"]!.ToString();
        else
            z = "https://localhost";
        if (new Uri(z).Scheme != "https")
            return Results.BadRequest();
        await CreateNoteMention(username, hostname, nx, ny, z);
        return Results.Ok();
    }
    if (t == "create_note_hashtag")
    {
        string y, z;
        if (!String.IsNullOrEmpty(send["url"]?.GetValue<string>()))
            y = send["url"]!.ToString();
        else
            y = "https://localhost";
        if (new Uri(y).Scheme != "https")
            return Results.BadRequest();
        if (!String.IsNullOrEmpty(send["tag"]?.GetValue<string>()))
            z = send["tag"]!.ToString();
        else
            z = "Hashtag";
        await CreateNoteHashtag(username, hostname, nx, y, z);
        return Results.Ok();
    }
    if (t == "update_note")
    {
        string y;
        if (!String.IsNullOrEmpty(send["url"]?.GetValue<string>()))
            y = send["url"]!.ToString();
        else
            y = $"https://{hostname}/u/{username}/s/00000000000000000000000000000000";
        if (new Uri(y).Scheme != "https")
            return Results.BadRequest();
        await UpdateNote(username, hostname, nx, y);
        return Results.Ok();
    }
    if (t == "delete_tombstone")
    {
        string y;
        if (!String.IsNullOrEmpty(send["url"]?.GetValue<string>()))
            y = send["url"]!.ToString();
        else
            y = $"https://{hostname}/u/{username}/s/00000000000000000000000000000000";
        if (new Uri(y).Scheme != "https")
            return Results.BadRequest();
        await DeleteTombstone(username, hostname, nx, y);
        return Results.Ok();
    }
    Console.WriteLine($"TYPE {aid} {atype}");
    return Results.Ok();
});

app.MapGet("/.well-known/nodeinfo", (HttpContext context) =>
{
    var hostname = new Uri(config["origin"]!.ToString()).Host;
    var body = new Dictionary<string, object>
    {
        { "links", new List<object>
            {
                new Dictionary<string, string>
                {
                    { "rel", "http://nodeinfo.diaspora.software/ns/schema/2.0" },
                    { "href", $"https://{hostname}/nodeinfo/2.0.json" }
                },
                new Dictionary<string, string>
                {
                    { "rel", "http://nodeinfo.diaspora.software/ns/schema/2.1" },
                    { "href", $"https://{hostname}/nodeinfo/2.1.json" }
                }
            }
        }
    };
    var headers = new Dictionary<string, string>
    {
        { "Cache-Control", $"public, max-age={config["ttl"]!.ToString()}, must-revalidate" },
        { "Vary", "Accept, Accept-Encoding" }
    };
    foreach (var header in headers)
        context.Response.Headers[header.Key] = header.Value;
    return Results.Json(body);
});

app.MapGet("/.well-known/webfinger", (HttpContext context) =>
{
    var username = config["actor"]?[0]?["preferredUsername"]!.ToString();
    var hostname = new Uri(config["origin"]!.ToString()).Host;
    var p443 = $"https://{hostname}:443/";
    var resource = context.Request.Query["resource"].ToString();
    var hasResource = false;
    if (resource.StartsWith(p443))
        resource = $"https://{hostname}/{resource.Substring(p443.Length)}";
    if (resource == $"acct:{username}@{hostname}")
        hasResource = true;
    if (resource == $"mailto:{username}@{hostname}")
        hasResource = true;
    if (resource == $"https://{hostname}/@{username}")
        hasResource = true;
    if (resource == $"https://{hostname}/u/{username}")
        hasResource = true;
    if (resource == $"https://{hostname}/user/{username}")
        hasResource = true;
    if (resource == $"https://{hostname}/users/{username}")
        hasResource = true;
    if (!hasResource)
        return Results.NotFound();
    var body = new Dictionary<string, object>
    {
        { "subject", $"acct:{username}@{hostname}" },
        { "aliases", new List<string>
            {
                $"mailto:{username}@{hostname}",
                $"https://{hostname}/@{username}",
                $"https://{hostname}/u/{username}",
                $"https://{hostname}/user/{username}",
                $"https://{hostname}/users/{username}"
            }
        },
        { "links", new List<object>
            {
                new Dictionary<string, string>
                {
                    { "rel", "self" },
                    { "type", "application/activity+json" },
                    { "href", $"https://{hostname}/u/{username}" }
                },
                new Dictionary<string, string>
                {
                    { "rel", "http://webfinger.net/rel/avatar" },
                    { "type", "image/png" },
                    { "href", $"https://{hostname}/static/{username}u.png" }
                },
                new Dictionary<string, string>
                {
                    { "rel", "http://webfinger.net/rel/profile-page" },
                    { "type", "text/plain" },
                    { "href", $"https://{hostname}/u/{username}" }
                }
            }
        }
    };
    var headers = new Dictionary<string, string>
    {
        { "Cache-Control", $"public, max-age={config["ttl"]!.ToString()}, must-revalidate" },
        { "Vary", "Accept, Accept-Encoding" }
    };
    foreach (var header in headers)
        context.Response.Headers[header.Key] = header.Value;
    return Results.Json(body, contentType: "application/jrd+json");
});

app.MapGet("/@", () => Results.Redirect("/"));
app.MapGet("/u", () => Results.Redirect("/"));
app.MapGet("/user", () => Results.Redirect("/"));
app.MapGet("/users", () => Results.Redirect("/"));

app.MapGet("/users/{username}", (string username) => Results.Redirect($"/u/{username}"));
app.MapGet("/user/{username}", (string username) => Results.Redirect($"/u/{username}"));
app.MapGet("/@{username}", (string username) => Results.Redirect($"/u/{username}"));

app.Run($"http://*:{port}");
